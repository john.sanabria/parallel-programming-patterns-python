# Parallel Programming Patterns Python

Hay múltiples niveles de patrones de diseño en paralelo que se pueden aplicar a un programa. 
Al nivel más alto, _algorithmic strategy patterns_ que permiten descomponer un problema en su forma más abstracta. 
Después, _implementation strategy patterns_ son técnicas prácticas para implementar ejecución paralela en el código fuente. 
Al nivel más bajo, _parallel execution patterns_ que indica como el software se ejecuta en arquitecturas de hardware paralelas específicas.

## _Algorithmic Strategy Patterns_

Inicialmente se debe identificar oportunidades para alcanzar concurrencia dentro de un programa.
Esto puede ser a través de *__task parallelism__* o *__data parallelism__*. 

* __task parallelism__ es descomponer un problema en sub-problemas los cuales se pueden ejecutar en paralelo. Se conoce a veces como _functional decomposition_. Ejemplo: es el _pipelining_.

* __data parallelism__ es ejecutar la misma operación sobre múltiples datos. Se conoce como _domain decomposition_. Ejemplo: es particionar los elementos de un vector para su posterior procesamiento.

# _Implementation strategy patterns_

Una vez se identifica el _algorithmic strategy for parallel execution_, se identifica ahora la técnicas para implementar el algoritmo en software.
Un ejemplo es el `fork-join`. El programa comienza con un solo hilo __principal__ y una vez se encuentra una tarea paralela se crean nuevos hilos y se ejecutan en paralelo. Una vez todos los hilos terminan y son destruidos el hilo __principal__ continúa con la ejecución. 

<img title="fork-join pattern https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ParallelDesign.html" src="images/fork-join.png"/> 

Este patrón es muy común con _data parallelism_ y _loop parallelism_.
Un ejemplo se puede ver en la librería OpenMP.
En este tipo de escenario los elementos del arreglo se dividen de manera arbitraria y se procesan en las divisiones que se establecen.

<img title="OpenMP https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ParallelDesign.html" src="images/openmp-code-segment.png"/> 

`Map/reduce` es otra estrategia similar al `fork-join`. 
En `map/reduce` las datos son procesados en paralelo por múltiples hilos y los resultados son colectados y mezclados a medida que los hilos terminan hasta llegar a una respuesta.

<img title="Map/Reduce https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ParallelDesign.html" src="images/map-reduce.png"/> 

Una estrategia de implementación común con _data parallelism_ es el `manager/worker`. 
En esta estrategia tareas independientes son distribuidas entre múltiples hilos de ejecución y se comunican únicamente a través del hilo _manager_.
El _manager_ monitorea la ejecución de las tareas y es el encargado de balancear la carga equitativamente.
Este patrón está más orientado a _task parallelism_.

<img title="Manager/Worker https://www.researchgate.net/publication/272419400_Architectural_Patterns_for_Parallel_Programming/figures?lo=1" src="images/manager-worker.png"/> 

## _Parallel Execution Patterns_

Una vez se establece la estrategia de implementación, el desiñeador de software necesita tomar decisiones respecto a como el software paralelo se ejecutará sobre el hardware disponible.
Una posible técnica es usar _thread pool_ con una _task queue_ asociada.
El _thread pool_ es un número determinado de hilos disponibles para procesar datos. 
Las tarea se ubican en la _task queue_. Cuando hay un hilo disponible y hay una tarea que requiere ser procesada entonces dicha tarea se remueve de la cola y se procesa.

<img title="thread pool/task queue https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ParallelDesign.html" src="images/thread_pool-task_queue.png"/> 

Una de las ventajas del patrón _thread pool_ es que los hilos son creados al momento de la ejecución del programa y luego se reutilizan a medida que se necesiten. 
Con esta aproximación hay un ahorro en el tiempo de manejo de los hilos.

Un ejemplo de este tipo de patrón de ejecución se puede encontrar en la concurrencia que usa Python, [este enlace](https://realpython.com/python-concurrency/#how-to-speed-up-an-io-bound-program).


## Enlaces

<https://www.stolaf.edu/people/rab/pdc/text/patterns.htm>

<https://w3.cs.jmu.edu/kirkpams/OpenCSF/Books/csf/html/ParallelDesign.html>
